# Front-end Developer

O desafio é bem simples, basta desenvolver uma interface consumindo os dados da [API da Marvel](https://developer.marvel.com) ou da [API do Pokémon](https://pokeapi.co/), use os dados de retorno da melhor forma que puder.

## Requisitos
- HTML Semântico
- SCSS e JS bem escritos
- Responsivo

## Extras
- ES6
- Vue.js (aumentará bemsua pontuação)

## Informações
- Utilizar a fonte [Montserrat](https://fonts.google.com/specimen/Montserrat) do Google
- Disponibilizar URL para a visualização do teste
- O código desenvolvido deve ser disponibilizado em um repositório

## Sugestão de Layout
### Pokémon
[Abrir no Adobe XD](https://xd.adobe.com/view/160fdf5e-d566-45b1-b2e2-1e3909164709-435c/?fullscreen)

![Pokémon](https://i.ibb.co/mzw4xvK/Pokemon.gif)

### Marvel
[Abrir no Adobe XD](https://xd.adobe.com/view/9a0c4733-cb9f-44d9-bcdd-d0341b2216a8-cf5f/?fullscreen)

![Marvel](https://i.ibb.co/S33Ch4x/Marvel.gif)

